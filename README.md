[Earth] Data Science
====================



This repository contains the frontal GitLab EDS materials. You will therefore find 
some bibliographical references and installation scripts. 

Syllabus is [here](Presentation_UE_EarthDataScience_2021.pdf)

Lectures and Labs (and associated data) are on the dedicated repository for each academic year.

Academic Y22-23 is now open. on https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/eds_22_23

The 2021-2022 is now closed.

The 2019-2020 and 2020-2021 are archived. 

------------
Installation
------------

### Contents of this repository

The first step is to clone the content of this repository
```
git clone https://pss-gitlab.math.univ-paris-diderot.fr/dralucas/earth-data-science
```
or by downloading the ZIP archive.

#### Environment

You will need to install the following libraries to make run all the codes

##### Basic scientific libraries

| [numpy](https://numpy.org/) | [scipy](https://www.scipy.org/) | [matplotlib](https://matplotlib.org/) | [pandas](https://pandas.pydata.org/) | [panda-profiling](https://pypi.org/project/pandas-profiling/) | [jupyter](https://jupyter.org/) |
| :-----: | :-----: | :----------: | :------: | :---------------: | :------------------------: |

##### ML/DL libraries

| [scikit-learn](https://scikit-learn.org/stable/index.html) | [keras](https://keras.io/) | [tensorflow](https://www.tensorflow.org/) |
| :------------: | :-----: | :----------: |



Below, you will find the basic instructions to install a conda environment. 

It's compatible with Windows, Linux and macOS.

0. Install Python distribution [Anaconda](https://www.anaconda.com/products/individual) or its little sister [Miniconda](https://docs.conda.io/en/latest/miniconda.html) 

1. Update your conda
    ```
    conda update -n base conda
    ```

2. Create a virtual environment called `earth-ds`
    ```
    cd earth-data-science
    conda env create -f binder/environmentEDS.yml
    ```

3. Activate your environment
    ```
    conda activate earth-ds   # 'source activate earth-ds' is also working.
    ```
4. Alternative to install miniconda and the libraries in one time.

    - You need first to create your environment.
    - Execute the script `install_miniconda_for_EDS.sh` saved in `Script` directory. 

##### Test your environment

Once your environment is activated, you can test it by running the following script:

```
python binder/test_import_lib.py
```

If the version of every libraries are displayed, the installation is correct !

