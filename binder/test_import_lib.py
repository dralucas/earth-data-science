import warnings

warnings.filterwarnings("ignore")

LIBRARY_NAMES = {
    "numpy": "NumPy",
    "scipy": "SciPy",
    "pandas": "Pandas",
    "matplotlib": "Matplotlib",
    "sklearn": "Scikit-learn",
    "keras": "Keras",
    "tensorflow": "TensorFlow",
    "obspy": "Obspy",
    "pandas_profiling": "Pandas-Profiling"
}

for lib, name in LIBRARY_NAMES.items():
    try:
        x = __import__(lib)
        print(f"{name}: {x.__version__}")

    except ImportError:
        print(f"⚠️ {name} could not be imported")

