wget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
chmod +x miniconda.sh
 ./miniconda.sh -b
export PATH=~/miniconda3/bin:$PATH
conda update --yes conda
conda install spyder scipy numpy matplotlib ipython jupyter scikit-learn pandas keras pandas-profiling
